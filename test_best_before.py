import unittest2
from best_before.best_before import create_possible_dates


class MyTest(unittest2.TestCase):
    def test_add(self):
        possible_dates = create_possible_dates("1/01/1")
        self.assertEquals(len(possible_dates), 1)
        possible_dates = create_possible_dates("09/01/2")
        self.assertEquals(len(possible_dates), 6)
if __name__ == '__main__':
    unittest2.main()
