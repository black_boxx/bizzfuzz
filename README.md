### What is this repository for? ###

This is my repository for Milo Solutions tasks:
1 Django BizzFuzz task
2 Python best_before task 

### How do I get set up? ###

* Summary of set up
1 STEP 
You need first of all to have pip and virtualenv on your machine installed. And then make some steps to run server:

2 STEP
cd ~/  
virtualenv milo-dev-test --no-site-packages  
cd milo-dev-test/bin/  
. activate  
git clone git@bitbucket.org:black_boxx/bizzfuzz.git  
cd bizzfuzz/  
pip install -r reqirements.txt  
python manage.py runserver  
  
Then open http://127.0.0.1:8000/ in your browser   
  
* Database configuration
There is already database, which i was made with migrations, so you can just run server. 
If you want, you can delete it (db.sqlite3) and then:
python manage.py syncdb

  
* Dependencies
all dependencies located in requirements.txt, you may install them with pip install -r reqirements.txt (as you made in 2 STEP) or by hands like pip install django==1.7
  
* How to run tests
there are 2 files to run tests
you can run them like this:
python test_bizz_fuzz.py
python test_best_before.py
  
RELEASE NOTE
- List of what was done and wasn’t, e.g. a release note.
I completed two tasks:
  
1 Django BizzFuzz task
I created django project with bizzbuzz app, so you can see a list of users, you can add them, delete, and change. To view a single user click on his username (with eye).If you want to edit or delete user, click on eye and edit and delete buttons will appear.
i also COMPLETED OPTIONAL TASK so any table now are downloadable with csv and xls icons. Example of file is located in SCREENSHOTS_AND_XLS_CSV folder.
  
2 Python best_before task 
I created python program which can be used to estimate earliest best_before date

Screenshots can be located in SCREENSHOTS_AND_XLS_CSV folder