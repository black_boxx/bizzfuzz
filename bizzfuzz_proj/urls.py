from django.conf.urls import patterns, include, url
from django.contrib import admin
from bizzfuzz_app.urls import urlpatterns as bizzpatterns


urlpatterns = patterns(
    '',
    url(r'^admin/', include(admin.site.urls)),
)
urlpatterns += bizzpatterns
