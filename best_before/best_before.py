#coding:utf-8
from sys import argv  # this we need to get a file from command line
import time
import sys


def init():
    try:
        script, input_file = argv
        datefile = open(input_file)
        dateline = datefile.readline()
        return dateline
    except ValueError:
        print ("NEED FILE! try 'python best_before.py date.txt' ")
        sys.exit(1)
        return

""" give_legal_date receive date and pattern to check it.
    Function will return date if it is real date,
    including leap year check and"""


def give_legal_date(input_date, pattern):
    try:
        valid_date = time.strptime(input_date, pattern)
        # print "ok", valid_date
        return valid_date
    except ValueError:
        # print "not match pattern or illegal date", input_date
        return  # if date is illegal , function will not return Date


def normalize_date(input_date, year_position):
    disassemled_digits = input_date.split('/')  # create digits list
    clean_year = replace_year(disassemled_digits[year_position])
    disassemled_digits[year_position] = clean_year  # replace year in gigits
    results = map(str, disassemled_digits)
    result_date = "/".join(results)  # assemble digits in single string again
    return result_date


"""  replace date with common XXXX format and
    set year between 2000 and 2999 """


def replace_year(str):
    try:
        year = int(str)
    except ValueError:
        return
    if year >= 2000 and year <= 2999:
        pass
    elif year > 0 and year <= 99:
        year += 2000
    else:
        return
    return year

""" Now we are creating array of
        all posible date combinations, we call normalize_date
        to get proper year syntax , then
        we remove all None's that we get from normalize_date """


def create_possible_dates(dateline):
    dates = []
    date = normalize_date(dateline, 0)
    clean_date = give_legal_date(date, '%Y/%m/%d')
    dates.append(clean_date)
    date = normalize_date(dateline, 0)
    clean_date = give_legal_date(date, '%Y/%d/%m')
    dates.append(clean_date)
    date = normalize_date(dateline, 1)
    clean_date = give_legal_date(date, '%d/%Y/%m')
    dates.append(clean_date)
    date = normalize_date(dateline, 2)
    clean_date = give_legal_date(date, '%d/%m/%Y')
    dates.append(clean_date)
    date = normalize_date(dateline, 2)
    clean_date = give_legal_date(date, '%m/%d/%Y')
    dates.append(clean_date)
    date = normalize_date(dateline, 1)
    clean_date = give_legal_date(date, '%m/%Y/%d')
    dates.append(clean_date)
    l = [i for i in dates if i is not None]
    s = set(l)
    return s
# IF someone call file
if __name__ == '__main__':
    dateline = init()
    possible_dates = create_possible_dates(dateline)
    try:
        earliest_date = min(possible_dates)
        formatted_date = time.strftime('%Y-%m-%d', earliest_date)
        print formatted_date
    except ValueError:
        print dateline, "is illegal"
