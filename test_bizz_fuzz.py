import unittest2
from bizzfuzz_app.templatetags.tags import get_bizzfuzz


class MyTest(unittest2.TestCase):
    def test_add(self):
        result = get_bizzfuzz(30)
        self.assertEquals("BizzFuzz", result)
        result = get_bizzfuzz(10)
        self.assertEquals("Fuzz", result)
        result = get_bizzfuzz(9)
        self.assertEquals("Bizz", result)
if __name__ == '__main__':
    unittest2.main()
