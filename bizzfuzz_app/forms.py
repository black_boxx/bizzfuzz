#coding:utf-8
from django.contrib.auth.forms import UserCreationForm
from bizzfuzz_app.models import CustomUser
from django import forms
from bizzfuzz_app.widgets import DatePickerWidget


class UserForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
            super(UserForm, self).__init__(*args, **kwargs)
            self.fields['birthday'] = forms.DateField(
                label='birthday',
                widget=DatePickerWidget()
            )

    class Meta:
        verbose_name = 'User'
        model = CustomUser
        fields = (
            'username',
            'birthday'
        )

    def save(self, commit=True):
        user = super(UserForm, self).save(commit=False)
        #user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(UserCreationForm):

    class Meta:
        verbose_name = u"User"
        model = CustomUser
        exclude = ['is_admin', 'last_visit', 'is_active', 'password', ]

    def clean_password(self):
        return self.initial["password"]

    def __init__(self, *args, **kwargs):
        super(UserChangeForm, self).__init__(*args, **kwargs)


class UserShowForm(forms.ModelForm):

    class Meta:
        verbose_name = u"User"
        model = CustomUser
        exclude = [
            'is_admin',
            'last_visit',
            'is_active',
            'password',
            'password1'
        ]

    def clean_password(self):
        return self.initial["password"]

    def __init__(self, *args, **kwargs):
        super(UserShowForm, self).__init__(*args, **kwargs)
