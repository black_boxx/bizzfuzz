#coding:utf-8
from django.template.response import TemplateResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django_tables2_reports.config import RequestConfigReport as RequestConfig
from bizzfuzz_app.tables import UserTable
from bizzfuzz_app.models import CustomUser
from bizzfuzz_app.forms import UserForm
from django.contrib import messages
from django.core.context_processors import csrf
from django.shortcuts import get_object_or_404
import random


def list(request,):
    all_users = CustomUser.objects.all()
    table = UserTable(all_users)
    table.order_by = "-id"
    RequestConfig(request).configure(table)
    table.paginate(page=request.GET.get('page', 1), per_page=10)
    return TemplateResponse(
        request,
        "list.html",
        {'table': table})


def add(request):
# Create a new_page
    if request.method == 'POST':
        form = UserForm(request.POST)
        if form.is_valid():
            new_item = form.save(commit=False)
            new_item.random_number = random.randint(0, 100)
            new_item.save()
            messages.success(request, "User created")
            return HttpResponseRedirect("/")
    else:
        form = UserForm()
    args = {}
    args.update(csrf(request))
    args['form'] = form
    return render_to_response(
        'add.html',
        args,
        context_instance=RequestContext(request)
    )


def view(request, username):
    user = CustomUser.objects.filter(username=username)
    table = UserTable(user)
    RequestConfig(request).configure(table)

    return TemplateResponse(
        request,
        "view.html",
        {'table': table, 'username': user[0].username, }
    )


def edit(request, username):
    user = get_object_or_404(CustomUser, username=username)
    # Send POST data for update an existing page.Update a page.
    if request.method == 'POST':
        form = UserForm(request.POST, instance=user)
        if form.is_valid():
            messages.success(
                request, u"User Updated "
            )
            new_item = form.save(commit=False)
            new_item.save()
            return HttpResponseRedirect('/')
    # Render a form to edit data for existing page
    else:
        form = UserForm(instance=user)
    args = {}
    args.update(csrf(request))
    args['form'] = form
    args['object'] = user
    return render_to_response(
        'edit.html',
        args,
        context_instance=RequestContext(request)
    )


def delete(request, username):
    user_to_del = get_object_or_404(CustomUser, username=username)
    user_to_del.delete()
    messages.success(request, "User deleted")
    return HttpResponseRedirect("/")
