#coding: utf-8
from django import forms
from django.contrib.staticfiles.storage import staticfiles_storage
from django.utils.safestring import mark_safe


class DatePickerWidget(forms.DateInput):
    class Media:
        css = {
            'all': (
                staticfiles_storage.url(
                    '/static/css/jquery-ui-1.8.17.custom.css'),)
        }
        js = (
            staticfiles_storage.url(
                '/static/js/jquery-ui-1.10.4.custom.min.js'),
        )

    def __init__(self, params='', attrs=None):
        self.params = params
        super(DatePickerWidget, self).__init__(attrs=attrs)

    def render(self, name, value, attrs=None):
        rendered = super(
            DatePickerWidget, self).render(name, value, attrs=attrs)
        return rendered + mark_safe(u'''<script type="text/javascript">
            $('#id_%s').datepicker({changeMonth: true, changeYear: true});
            </script>''' % (name))
