from django.conf.urls import patterns, url

#AJAX LOOKUP
from django.conf.urls import *

urlpatterns = patterns(
    '',
    url(
        r'^accounts/login$',
        'django.contrib.auth.views.login'
    ),
    url(r'^$', "bizzfuzz_app.views.list"),
    url(r'^add/$', "bizzfuzz_app.views.add"),
    url(r'^view/(?P<username>\w+)/$', "bizzfuzz_app.views.view"),
    url(r'^edit/(?P<username>\w+)/$', "bizzfuzz_app.views.edit"),
    url(r'^delete/(?P<username>\w+)/$', "bizzfuzz_app.views.delete"),

)
