#coding:utf-8
from django import template
register = template.Library()
from datetime import date


@register.filter
def get_bizzfuzz(number=None):
    if number is not None:
        if number % 5 == 0 and number % 3 == 0:
            string = "BizzFuzz"
            return string
        if number % 3 == 0:
            string = "Bizz"
            return string
        if number % 5 == 0:
            string = "Fuzz"
            return string
        else:
            return ""
    else:
        return ""


def calculate_age(born):
    today = date.today()
    return today.year - born.year
    - ((today.month, today.day) < (born.month, born.day))


@register.filter
def get_eligible(birthday):
    if birthday is not None:
        age = calculate_age(birthday)
        if age > 13:
            return "allowed"
        else:
            return "not allowed"
        return age
    else:
        return "unknown(no birthday provided)"


@register.filter
def get_field_value(obj, field_name):
    return getattr(obj, field_name, None)


@register.filter
def get_verbose_name(obj, field_name):
    return obj._meta.get_field(field_name).verbose_name

register.tag('get_verbose_name', get_verbose_name)
register.tag('get_field_value', get_field_value)


@register.filter
def to_class_name(value):
    return value.__class__.__name__

register.tag('to_class_name', to_class_name)
