#coding:utf-8
from django_tables2.columns import TemplateColumn
from django_tables2_reports.tables import TableReport
from bizzfuzz_app.models import CustomUser
import itertools
from django.utils.safestring import mark_safe


class UserTable(TableReport):
    BizzFuzz = TemplateColumn(
        '{% load tags %}{{ record.random_number|get_bizzfuzz }}'
    )
    Eliglible = TemplateColumn(
        '{% load tags %}{{ record.birthday|get_eligible }}'
    )

    def __init__(self, *args, **kwargs):
        super(UserTable, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    def render_username(self, value):
        return mark_safe(
            '<a class="button fa fa-eye fa-2x" href="/view/%s"> %s</a>'
            % (value, value)
        )

    class Meta:
        model = CustomUser
        exclude = ['last_login', 'is_active', 'is_admin', 'password', 'id']
        attrs = {"class": "paleblue"}
